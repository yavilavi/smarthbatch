# Generated by Django 3.0.6 on 2020-07-02 03:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0023_auto_20200627_2305'),
        ('user', '0003_user_cellphone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='area',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='erp.Process', verbose_name='Proceso'),
        ),
        migrations.AlterField(
            model_name='user',
            name='cellphone',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='N° Celular'),
        ),
    ]
