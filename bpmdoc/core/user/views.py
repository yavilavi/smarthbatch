from datetime import datetime

from django.contrib.auth import update_session_auth_hash, login
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

from core.erp.mixins import ValidatePermissionRequiredMixin
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, FormView

from core.erp.models import Document
from core.user.forms import UserForm, UserProfileForm, UserProfileConsultForm, RegisterForm
from core.user.models import User


class UserListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = User
    template_name = 'user/list.html'
    permission_required = 'user.view_user'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in User.objects.filter(is_superuser=False).order_by('first_name'):
                    data.append(i.tojson())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Usuarios'
        context['create_url'] = reverse_lazy('user:user_create')
        context['list_url'] = reverse_lazy('user:user_list')
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['entity'] = 'Usuarios'
        return context


class UserCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = User
    form_class = UserForm
    template_name = 'user/create.html'
    success_url = reverse_lazy('user:user_list')
    permission_required = 'user.add_user'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        global form
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_username = form.cleaned_data.get ('username')
                    messages.success (request, F'Usuario {name_username} creado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Creación de Usuarios'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['entity'] = 'Crear Usuario'
        return context


class UserUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = User
    form_class = UserForm
    template_name = 'user/create.html'
    success_url = reverse_lazy('user:user_list')
    permission_required = 'user.change_user'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        global form
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_username = form.cleaned_data.get ('username')
                    messages.success (request, f'Usuario {name_username} actualizado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Edición de Usuarios'
        context['list_url'] = self.success_url
        context['entity'] = 'Editar de Usuario'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context


class UserProfileView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserProfileForm
    template_name = 'user/profile.html'
    success_url = reverse_lazy('user:user_profile_ver')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def post(self, request, *args, **kwargs):
        global form
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_username = form.cleaned_data.get ('username')
                    messages.success (request, f'Usuario "{name_username}", editado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Edición de Perfil'
        context['list_url'] = self.success_url
        context['entity'] = 'Editar Perfil'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context


class UserProfileConsultView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserProfileConsultForm
    template_name = 'user/detail.html'
    success_url = reverse_lazy('erp:inicio')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Perfil de Usuario'
        context['list_url'] = self.success_url
        context['entity'] = 'Perfil de Usuario'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context


class UserChangePasswordView(LoginRequiredMixin, FormView):
    model = User
    form_class = PasswordChangeForm
    template_name = 'user/change_password.html'
    success_url = reverse_lazy('user:change_password')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
       return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = PasswordChangeForm(user=self.request.user)
        form.fields['old_password'].widget.attrs['class'] = 'form-control'
        form.fields['new_password1'].widget.attrs['class'] = 'form-control'
        form.fields['new_password2'].widget.attrs['class'] = 'form-control'
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = PasswordChangeForm(user=request.user, data=request.POST)
                if form.is_valid():
                    form.save()
                    update_session_auth_hash(request, form.user)
                    messages.success(request, f'Su contraseña fue actualizada satisfactoriamente!')
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Contraseña'
        context['list_url'] = self.success_url
        context['entity'] = 'Editar Contraseña'
        context['action'] = 'edit'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['inicio_url'] = reverse_lazy('erp:inicio')
        return context


def registerview(request):
    form = RegisterForm()
    initial_dict = {'group': 'Administrador'}
    if request.method == 'POST':
        form = RegisterForm(data=request.POST, initial=initial_dict)
        if form.is_valid():
            usuario = form.save()
            group = Group.objects.get(name='Administrador')
            usuario.groups.add(group)
            name_username = form.cleaned_data.get('username')
            login(request, usuario)
            messages.success(request, f"El usuario {name_username}, ha sido creado correctamente!")
            return redirect('erp:inicio')
        else:
            for msg in form.errors:
                messages.error(request, form.errors[msg])
    context = {'form': form,
               'tittle': 'Crear Cuenta'}
    return render(request, 'register/register.html', context)
