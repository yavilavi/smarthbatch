from django.urls import path
from core.user.views import *

app_name = 'user'

urlpatterns = [
    path('list/', UserListView.as_view(), name='user_list'),
    path('add/', UserCreateView.as_view(), name='user_create'),
    path('update/<int:pk>/', UserUpdateView.as_view(), name='user_update'),
    path('profile/', UserProfileView.as_view(), name='user_profile'),
    path('profile_ver/', UserProfileConsultView.as_view(), name='user_profile_ver'),
    path('edit/password/', UserChangePasswordView.as_view(), name='change_password'),
    path('register/', registerview, name='account_create')
]
