var tblUsers;
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr class="text-md-center" style="background-color: #858796">'+
            '<td style="color: white"><b>Staff</b></td>'+
            '<td style="color: white"><b>Ultima sesión</b></td>'+
            '<td style="color: white"><b>Cedula</b></td>'+
        '</tr>'+
        '<tr class="text-md-center">'+
            '<td>'+d.is_staff+'</td>'+
            '<td>'+d.last_login+'</td>'+
            '<td>'+d.cedula+'</td>'+
        '</tr>'+
    '</table>';
}

$(function ()  {
    tblUsers = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {'data': 'full_name'},
            {'data': 'username'},
            {'data': 'cargo'},
            {'data': 'groups'},
            {'data': 'email'},
            {'data': 'is_active'},
            {'data': 'is_active'},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/user/update/' + row.id + '/" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-edit"></i></a>';
                    buttons;
                    return buttons;
                }
            },
            {
                targets: [-2,-3,-5,-6,-7],
                class: 'align-middle'
            },
            {
                targets: [-4],
                class: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    $.each(row.groups, function (key, value) {
                        html+='<span class="badge badge-success">'+value.name+'</span> '
                    });
                    return html
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
        $('#data tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblUsers.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
});