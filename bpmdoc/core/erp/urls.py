from django.urls import path

from core.erp.views.changes.views import *
from core.erp.views.inicio.views import *
from core.erp.views.process.views import *
from core.erp.views.typedoc.views import *
from core.erp.views.docs.views import *
from core.homepage.views import SinpermisosView

# url media files
from django.conf import settings
from django.conf.urls.static import static


app_name = 'erp'

urlpatterns = [
    # Tipos de documento
    path('typedoc/list/', TypedocListView.as_view(), name='typedoc_list'),
    path("typedoc/add/", TypeCreateView.as_view(), name='type_create'),
    path('typedoc/update/<int:pk>/', TypeUpdateView.as_view(), name='type_update'),
    path('typedoc/delete/<int:pk>/', TypeDeleteView.as_view(), name='type_delete'),
    path('typedoc/form/', TypeFormView.as_view(), name='type_form'),

    # Documentos
    path('docs/list/', DocListView.as_view(), name='docs_list'),
    path('docs/review_list/', DocRevListView.as_view(), name='docs_review_list'),
    path('docs/approve_list/', DocAproListView.as_view(), name='docs_aprove_list'),
    path('docs/created_list/', DocCreatedListView.as_view(), name='docs_created_list'),
    path('docs/gestion_list/', DocGestListView.as_view(), name='docs_gest_list'),
    path('docs/adjust_update/<int:pk>/', DocAdjustUpdateView.as_view(), name='docs_adjust_update'),
    path('docs/review_update/<int:pk>/', DocRevUpdateView.as_view(), name='docs_review_update'),
    path('docs/approve_update/<int:pk>/', DocAproUpdateView.as_view(), name='docs_aprove_update'),
    path('docs/devolut_update/<int:pk>/', DocDevUpdateView.as_view(), name='docs_devolut_update'),
    path('docs/consulta/', ConsulDocListView.as_view(), name='consultdocs_list'),
    path('docs/add/', DocCreateView.as_view(), name='docs_create'),
    path('docs/update/<int:pk>/', DocUpdateView.as_view(), name='docs_update'),
    path('docs/delete/<int:pk>/', DocDeleteView.as_view(), name='docs_delete'),
    path('docs/detail/<int:pk>', DocsDetailView.as_view(), name='docs_detail'),
    path('docs/form/', DocFormView.as_view(), name='docs_form'),
    path('docs/listado_maestros_docs/', ReportDocsExcel.as_view(), name='list_master_docs'),

    # Procesos
    path('process/list/', ProcessListView.as_view(), name='process_list'),
    path('process/add/', ProcessCreateView.as_view(), name='process_create'),
    path('process/update/<int:pk>/', ProcessUpdateView.as_view(), name='process_update'),
    path('process/delete/<int:pk>/', ProcessDeleteView.as_view(), name='process_delete'),
    path('process/form/', ProcessFormView.as_view(), name='process_form'),

    # Cambios de documentos/ Control de Cambios
    path('changes/update/<int:id_change>/', document_change, name='changes_control'),

    # Inicio
    path('inicio/', InicioView.as_view(), name='inicio'),
    path('inicio/sinpermisos/', SinpermisosView.as_view(), name='sinpermisos'),

]

urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT)
