# Generated by Django 3.1.2 on 2020-10-26 00:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0110_auto_20201025_1930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='date_approved',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Fecha de Aprobado'),
        ),
        migrations.AlterField(
            model_name='document',
            name='date_revised',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Fecha de Revisado'),
        ),
    ]
