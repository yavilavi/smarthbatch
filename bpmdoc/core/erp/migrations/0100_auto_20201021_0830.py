# Generated by Django 3.1.2 on 2020-10-21 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0099_auto_20201020_1529'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='anexo_dos',
            field=models.FileField(blank=True, max_length=200, null=True, upload_to='anexos_dos/%Y/%m/%d', verbose_name='Anexo 2'),
        ),
        migrations.AlterField(
            model_name='document',
            name='fileword',
            field=models.FileField(blank=True, max_length=200, upload_to='docsword/%Y/%m/%d', verbose_name='Documento Word o Excel editable'),
        ),
    ]
