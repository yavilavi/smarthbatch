# Generated by Django 3.1.2 on 2020-10-26 00:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0109_auto_20201025_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='date_approved',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Fecha de Aprobado'),
        ),
        migrations.AlterField(
            model_name='document',
            name='date_revised',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Fecha de Revisado'),
        ),
    ]
