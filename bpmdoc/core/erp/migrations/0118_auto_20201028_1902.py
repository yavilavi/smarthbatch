# Generated by Django 3.1.2 on 2020-10-29 00:02

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0117_auto_20201028_0232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='date_alert_exp',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 10, 28, 19, 2, 48, 314008), null=True, verbose_name='Fecha Notificacion Proximo a Vencer'),
        ),
        migrations.AlterField(
            model_name='document',
            name='date_vigencia',
            field=models.DateField(blank=True, default=datetime.datetime(2020, 10, 28, 19, 2, 48, 314008), null=True, verbose_name='Vencimiento de Documento'),
        ),
    ]
