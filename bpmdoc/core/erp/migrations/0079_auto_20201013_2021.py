# Generated by Django 3.1.2 on 2020-10-14 01:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('erp', '0078_auto_20201013_1502'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='company/%Y/%m/%d', verbose_name='Cambiar Logo'),
        ),
        migrations.AlterField(
            model_name='client',
            name='nit',
            field=models.IntegerField(unique=True, verbose_name='NIT'),
        ),
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.CharField(blank=True, max_length=12, null=True, verbose_name='Telefono'),
        ),
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.IntegerField(default=1, max_length=100, unique=True, verbose_name='N° de Documento'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Aprobado por'),
        ),
        migrations.AlterField(
            model_name='document',
            name='version',
            field=models.IntegerField(default=1, verbose_name='Versión N°'),
        ),
    ]
