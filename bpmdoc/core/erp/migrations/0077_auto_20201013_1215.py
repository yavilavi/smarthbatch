# Generated by Django 3.1.2 on 2020-10-13 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0076_auto_20201013_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.CharField(blank=True, default=1051230, max_length=100, null=True, unique=True, verbose_name='N° de Documento'),
        ),
    ]
