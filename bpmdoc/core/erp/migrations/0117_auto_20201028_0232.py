# Generated by Django 3.1.2 on 2020-10-28 07:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0116_auto_20201028_0222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='time_vigencia',
            field=models.CharField(blank=True, choices=[('365', '1 año'), ('730', '2 años'), ('1095', '3 años'), ('1460', '4 años'), ('1825', '5 años')], max_length=30, null=True, verbose_name='Tiempo de Vigencia'),
        ),
    ]
