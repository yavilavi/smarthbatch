# Generated by Django 3.1.2 on 2020-10-29 02:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0124_auto_20201028_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='time_vigencia',
            field=models.CharField(blank=True, choices=[('365', '1 año'), ('730', '2 años'), ('1095', '3 años'), ('1461', '4 años'), ('1825', '5 años')], max_length=10, null=True, verbose_name='Tiempo de Vigencia'),
        ),
    ]
