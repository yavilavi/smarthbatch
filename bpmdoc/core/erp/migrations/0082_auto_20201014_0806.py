# Generated by Django 3.1.2 on 2020-10-14 13:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('erp', '0081_auto_20201013_2148'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='date_creation',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='client',
            name='user_creation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='erp_client_creation', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='client',
            name='user_updated',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='erp_client_updated', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.IntegerField(unique=True, verbose_name='N° de Documento'),
        ),
    ]
