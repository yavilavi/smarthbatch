# Generated by Django 3.0.6 on 2020-06-20 01:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0018_auto_20200616_2329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='changes',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='Cambios Realizados'),
        ),
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.CharField(blank=True, max_length=20, null=True, unique=True, verbose_name='Número de Cambio'),
        ),
        migrations.AlterField(
            model_name='document',
            name='status',
            field=models.CharField(choices=[('En Elaboracion', 'En Elaboración'), ('En Revisión', 'En Revisión'), ('En Aprobación', 'En Aprobación'), ('Vigente', 'Vigente'), ('Obsoleto', 'Obsoleto'), ('En Prueba', 'En Prueba')], default='En Elaboración', max_length=100, verbose_name='Estado'),
        ),
    ]
