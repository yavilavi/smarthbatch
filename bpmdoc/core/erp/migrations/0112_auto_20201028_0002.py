# Generated by Django 3.1.2 on 2020-10-28 05:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0111_auto_20201025_1941'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='time_vigencia',
            field=models.CharField(blank=True, choices=[('365', '1 año'), ('730', '2 años'), ('1095', '3 años'), ('1460', '4 años'), ('1825', '5 años')], max_length=30, null=True, verbose_name='Tiempo de Vigencia'),
        ),
        migrations.AlterField(
            model_name='document',
            name='date_vigencia',
            field=models.DateField(blank=True, null=True, verbose_name='Vencimiento de Documento'),
        ),
        migrations.AlterField(
            model_name='document',
            name='fileword',
            field=models.FileField(blank=True, help_text='.doc, .docx, .xls, .xlsx', max_length=200, upload_to='docsword/%Y/%m/%d', verbose_name='Documento editable'),
        ),
    ]
