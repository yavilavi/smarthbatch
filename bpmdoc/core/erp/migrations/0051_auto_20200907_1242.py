# Generated by Django 3.0.6 on 2020-09-07 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0050_auto_20200906_1151'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='date_revised',
            field=models.DateField(auto_now=True, verbose_name='Fecha de Revisado'),
        ),
        migrations.AlterField(
            model_name='document',
            name='approval',
            field=models.BooleanField(blank=True, default=False, verbose_name='Aprobar Documento'),
        ),
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.CharField(blank=True, default=1023340, max_length=100, null=True, unique=True, verbose_name='N° de Documento'),
        ),
        migrations.AlterField(
            model_name='document',
            name='review',
            field=models.BooleanField(blank=True, default=False, verbose_name='Aprobar Revisión'),
        ),
    ]
