# Generated by Django 3.0.5 on 2020-05-04 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='type',
            name='typedoc',
            field=models.CharField(max_length=30, verbose_name='Descripción'),
        ),
        migrations.AlterModelTable(
            name='state',
            table='Estado',
        ),
    ]
