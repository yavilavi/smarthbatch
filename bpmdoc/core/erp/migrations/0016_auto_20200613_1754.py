# Generated by Django 3.0.6 on 2020-06-13 22:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0015_remove_document_client'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='date_aproved',
        ),
        migrations.RemoveField(
            model_name='document',
            name='date_review',
        ),
    ]
