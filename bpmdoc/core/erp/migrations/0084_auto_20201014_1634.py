# Generated by Django 3.1.2 on 2020-10-14 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0083_remove_document_empresa'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.IntegerField(blank=True, null=True, unique=True, verbose_name='N° de Documento'),
        ),
    ]
