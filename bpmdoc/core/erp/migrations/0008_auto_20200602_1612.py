# Generated by Django 3.0.6 on 2020-06-02 21:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0007_auto_20200513_1207'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='client',
        ),
        migrations.AddField(
            model_name='document',
            name='client',
            field=models.CharField(default=1, max_length=200, verbose_name='Cliente'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='file',
            field=models.FileField(max_length=200, upload_to='code/%Y/%m/%d/', verbose_name='Documento'),
        ),
        migrations.RemoveField(
            model_name='document',
            name='state',
        ),
        migrations.AddField(
            model_name='document',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='erp.State', verbose_name='Estado'),
        ),
    ]
