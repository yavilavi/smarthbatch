# Generated by Django 3.1.2 on 2020-10-13 16:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0075_auto_20201012_2321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='numberchange',
            field=models.CharField(blank=True, default=1063946, max_length=100, null=True, unique=True, verbose_name='N° de Documento'),
        ),
        migrations.AlterField(
            model_name='document',
            name='version',
            field=models.IntegerField(blank=True, null=True, verbose_name='Versión N°'),
        ),
    ]
