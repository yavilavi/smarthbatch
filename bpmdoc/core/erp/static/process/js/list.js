$(function ()  {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {'data': 'process'},
            {'data': 'codeprocess'},
            {'data': 'leader.full_name'},
            {'data': 'leader.cargo'},
            {'data': 'codeprocess'},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/process/update/' + row.id + '/" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-edit"></i></a>';
                    buttons;
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});