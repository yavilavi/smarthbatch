var tblType;
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+

        '<tr class="text-md-center" style="background-color: #858796">'+
            '<td style="color: white"><b>Creado por</b></td>'+
            '<td style="color: white"><b>Editado por</b></td>'+
            '<td style="color: white"><b>Fecha de Creación</b></td>'+
            '<td style="color: white"><b>Fecha de Edición</b></td>'+
        '</tr>'+
        '<tr class="text-md-center">'+
            '<td>'+d.user_creation+'</td>'+
            '<td>'+d.user_updated+'</td>'+
            '<td>'+d.date_creation+'</td>'+
            '<td>'+d.date_updated+'</td>'+
        '</tr>'+
    '</table>';
}

$(function ()  {
    tblType = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {'data': 'typedoc'},
            {'data': 'codetype'},
            {'data': 'validity'},
            {'data': 'validity'},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/typedoc/update/' + row.id + '/" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-edit"></i></a>';
                    buttons;
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
        $('#data tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblType.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
});
