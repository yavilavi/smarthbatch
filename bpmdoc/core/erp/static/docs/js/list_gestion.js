var tblDocs;
$(function ()  {
    tblDocs = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {'data': 'process'},
            {'data': 'code'},
            {'data': 'description'},
            {'data': 'version'},
            {'data': 'tipodoc.typedoc'},
            {'data': 'fileword'},
            {'data': 'status'},
            {'data': 'proceso.process'},
            {'data': 'process'},
        ],
        columnDefs: [
            {
                targets: [-9],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    return '<a class="fas fa-info-circle fa-lg" href="/erp/docs/detail/' + row.id + '">';
                }
            },
            {
                targets: [-8,-7,-6,-5,-3,-2],
                className: 'align-middle',
            },
            {
                targets: [-4],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    if(row.fileword.length === 0){
                        return '';
                    } else if(row.anexo_dos.length === 0 && row.anexo.length === 0){
                       return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="' + row.fileword + '">';
                    }else if(row.anexo.length === 0){
                    return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="'+row.filepdf+'">'
                        + "&nbsp" + '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="'+row.fileword+'">'
                        + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#2C3E50" href="' + row.anexo_dos + '">';
                    } else if(row.anexo_dos.length === 0) {
                        return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="' + row.fileword + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#808B96" href="' + row.anexo + '">';
                    } else {
                        return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="' + row.fileword + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#808B96" href="' + row.anexo + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#2C3E50" href="' + row.anexo_dos + '">';
                    }
                }
            },
            {
                targets: [-1],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/changes/update/' + row.id + '/" class="btn btn-info btn-circle btn-sm"><i class="fas fa-exchange-alt"></i></a>';
                    buttons;
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
