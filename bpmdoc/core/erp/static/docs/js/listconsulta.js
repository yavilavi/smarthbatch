var tblDocs;
$(function ()  {
    tblDocs = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {'data': 'process'},
            {'data': 'code'},
            {'data': 'description'},
            {'data': 'version'},
            {'data': 'tipodoc.typedoc'},
            {'data': 'filepdf'},
            {'data': 'status'},
            {'data': 'proceso.process'},
        ],
        columnDefs: [
            {
                targets: [-8],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    return '<a class="fas fa-info-circle fa-lg" href="/erp/docs/detail/' + row.id + '">';
                }
            },
            {
                targets: [-7,-6,-5,-4,-2,-1],
                className: 'align-middle',
            },
            {
                targets: [-3],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    if(row.filepdf.length === 0){
                        return '';
                    } else if(row.anexo_dos.length === 0 && row.anexo.length === 0){
                       return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">';
                    }else if(row.anexo.length === 0){
                    return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="'+row.filepdf+'">'
                        + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#2C3E50" href="' + row.anexo_dos + '">';
                    } else if(row.anexo_dos.length === 0) {
                        return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#808B96" href="' + row.anexo + '">';
                    } else {
                        return '<a target="_blank" class="far fa-file-pdf fa-lg" style="color:#e9322d" href="' + row.filepdf + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#808B96" href="' + row.anexo + '">'
                            + "&nbsp" + '<a target="_blank" class="far fa-file fa-lg" style="color:#2C3E50" href="' + row.anexo_dos + '">';
                    }
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
