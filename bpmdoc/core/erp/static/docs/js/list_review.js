var tblDocs;
   $(function ()  {
    tblDocs = $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {'data': 'code'},
            {'data': 'description'},
            {'data': 'version'},
            {'data': 'tipodoc.typedoc'},
            {'data': 'fileword'},
            {'data': 'status'},
            {'data': 'proceso.process'},
            {'data': 'process'},
            {'data': 'process'},
        ],
        columnDefs: [
            {
                targets: [-9,-8,-7,-6,-4,-3],
                className: 'align-middle',
            },
            {
                targets: [-5],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    if(row.fileword.length === 0){
                        return 'Sin archivo';
                    }
                    return '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="'+row.fileword+'">';
                }
            },
            {
                targets: [-2],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    if (row['status'] === 'En Aprobación') {
                    var buttons =
                        '<a href="/erp/docs/approve_update/' + row.id + '/" class="btn btn-success btn-circle btn-sm"><i class="fas fa-check"></i></a>';
                    buttons;
                    return buttons;
                    } else {
                    buttons =
                        '<a href="/erp/docs/review_update/' + row.id + '/" class="btn btn-success btn-circle btn-sm"><i class="fas fa-check"></i></a>';
                    buttons;
                    return buttons;
                    }
                }
            },
            {
                targets: [-1],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var buttons =
                        //'<a href="#" rel="edit" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                        '<a href="/erp/docs/devolut_update/' + row.id + '/" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    buttons;
                    return buttons;

                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});

