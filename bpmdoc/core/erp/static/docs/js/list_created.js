var tblDocs;
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr class="text-md-center" style="background-color: #858796">'+
            '<td style="color: white"><b>N° de Documento</b></td>'+
            '<td style="color: white"><b>Cambios Realizados</b></td>'+
            '<td style="color: white"><b>Justificación del Cambio</b></td>'+
        '</tr>'+
        '<tr class="text-md-center">'+
            '<td>'+d.numberchange+'</td>'+
            '<td>'+d.changes+'</td>'+
            '<td>'+d.justification+'</td>'+
        '</tr>'+
    '</table>';
}
$(function ()  {
    tblDocs = $('#data').DataTable({
        responsive: true,
        //scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {'data': 'code'},
            {'data': 'description'},
            {'data': 'tipodoc.typedoc'},
            {'data': 'fileword'},
            {'data': 'status'},
            {'data': 'comment_review'},
            {'data': 'process'},
        ],
        columnDefs: [
            {
                targets: [-7,-6,-5,-3,-2],
                className: 'align-middle',
            },
            {
                targets: [-4],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    if(row.fileword.length === 0){
                        return 'Sin archivo';
                    }
                    return '<a target="_blank" class="far fa-file-word fa-lg" style="color:#425CF0" href="'+row.fileword+'">';
                }
            },
            {
                targets: [-1],
                className: 'align-middle',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/docs/adjust_update/' + row.id + '/" class="btn btn-success btn-circle btn-sm"><i class="fas fa-edit"></i></a>';
                    buttons;
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
        $('#data tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblDocs.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
});
