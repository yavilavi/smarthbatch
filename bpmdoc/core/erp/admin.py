from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import *


class DocumentResource(resources.ModelResource):
    class Meta:
        model = Document

class DocumentAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    search_fields = ('code', 'description', 'tipodoc', 'proceso', 'version', 'creator', 'reviewer', 'owner', 'status',
                     'changes', 'justification', 'date_creation', 'date_revised', 'date_approved', 'date_vigencia')
    list_display = ('code', 'description', 'tipodoc', 'proceso', 'version', 'creator', 'reviewer', 'owner', 'status',
                    'changes', 'justification', 'date_creation', 'date_revised', 'date_approved', 'date_vigencia')
    resource_class = DocumentResource


# Register your models here.
admin.site.register(Document, DocumentAdmin)
admin.site.register(Type)
admin.site.register(Process)
