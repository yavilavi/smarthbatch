from crum import get_current_user
from django.core.validators import FileExtensionValidator

from django.db import models
from django.forms import model_to_dict
from simple_history.models import HistoricalRecords

from core.models import BaseModel
from djchoices import ChoiceItem, DjangoChoices
from config.settings import MEDIA_URL
from core.user.models import User
from datetime import datetime, timedelta


class Type(BaseModel):
    typedoc = models.CharField(max_length=60, verbose_name='Descripción', unique=True)
    codetype = models.CharField(max_length=10, verbose_name='Código', unique=True)
    validity = models.IntegerField(verbose_name='Años de Vigencia')
    history = HistoricalRecords(custom_model_name=lambda x:f'Audit_{x}')

    def __str__(self):
        return self.typedoc

    def toJSON(self):
        item = model_to_dict(self)
        item['user_creation'] = self.user_creation.__str__()
        item['user_updated'] = self.user_updated.__str__()
        item['date_creation'] = self.date_creation.strftime('%Y-%m-%d')
        item['date_updated'] = self.date_updated.strftime('%Y-%m-%d')
        return item

    class Meta:
        db_table = 'Tipo_Documento'
        verbose_name = 'Tipo de Documento'
        verbose_name_plural = 'Tipos de Documentos'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, *args, **kwargs):
        user = get_current_user()
        if user is not None:
            if not self.pk:
                self.user_creation = user
            else:
                self.user_updated = user
        self.typedoc = self.typedoc.capitalize()
        self.codetype = self.codetype.upper()
        return super(Type, self).save(*args, **kwargs)


class VigenciaDoc(DjangoChoices):
    one_year = ChoiceItem('365', "1 año")
    two_year = ChoiceItem('730', "2 años")
    three_year = ChoiceItem('1095', "3 años")
    four_year = ChoiceItem('1461', "4 años")
    five_year = ChoiceItem('1825', "5 años")


class StatusDoc(DjangoChoices):
    elaboracion = ChoiceItem('En Elaboración', "En Elaboración")
    revision = ChoiceItem("En Revisión", "En Revisión")
    aprobacion = ChoiceItem("En Aprobación", "En Aprobación")
    aprobado = ChoiceItem("Aprobado", "Aprobado")
    vigente = ChoiceItem("Vigente", "Vigente")
    vencido = ChoiceItem("Vencido", "Vencido")
    obsoleto = ChoiceItem("Obsoleto", "Obsoleto")
    prueba = ChoiceItem("En Prueba", "En Prueba")
    anulado = ChoiceItem("Anulado", "Anulado")


class Document(BaseModel):
    code = models.CharField(max_length=15, verbose_name='Código')
    description = models.CharField(max_length=200, verbose_name='Descripción')
    version = models.IntegerField(default=1, verbose_name='Versión N°')
    tipodoc = models.ForeignKey('Type', verbose_name='Tipo de Documento', on_delete=models.CASCADE)
    creator = models.ForeignKey(User, blank=False, verbose_name='Creado por', on_delete=models.CASCADE,
                                related_name='+')
    reviewer = models.ForeignKey(User, blank=False, verbose_name='Revisado por', on_delete=models.CASCADE,
                                 related_name='+')
    owner = models.ForeignKey(User, blank=False, verbose_name='Aprobado por', on_delete=models.CASCADE,
                              related_name='+')
    proceso = models.ForeignKey('Process', verbose_name='Proceso', on_delete=models.CASCADE)
    filepdf = models.FileField(upload_to='docspdf/%Y/%m/%d', max_length=200, blank=True,
                               verbose_name='Documento en PDF', validators=[FileExtensionValidator(['pdf'])])
    fileword = models.FileField(upload_to='docsword/%Y/%m/%d', max_length=200, blank=True,
                                verbose_name='Documento editable', validators=[FileExtensionValidator(['xls', 'xlsx', 'doc', 'docx'])])
    anexo = models.FileField(upload_to='anexos/%Y/%m/%d', max_length=200, blank=True, null=True, verbose_name='Anexo 1')
    anexo_dos = models.FileField(upload_to='anexos_dos/%Y/%m/%d', max_length=200, blank=True, null=True,
                                 verbose_name='Anexo 2')
    status = models.CharField(max_length=100, default='En Elaboración', choices=StatusDoc.choices,
                              verbose_name='Estado')
    changes = models.CharField(max_length=1000, null=True, blank=True, verbose_name='Cambios Realizados')
    justification = models.CharField(max_length=1000, null=True, blank=True, verbose_name='Justificación del Cambio')
    numberchange = models.IntegerField(unique=True, null=True, blank=True, verbose_name='N° de Documento')
    review = models.BooleanField(default=False, null=True, blank=True, verbose_name='Aprobar Revisión')
    approval = models.BooleanField(default=False, null=True, blank=True, verbose_name='Aprobar Documento')
    date_revised = models.DateField(default=None, blank=True, null=True, verbose_name='Fecha de Revisado')
    date_approved = models.DateField(default=None, blank=True, null=True, verbose_name='Fecha de Aprobado')
    time_vigencia = models.CharField(max_length=10, choices=VigenciaDoc.choices, blank=True, null=True,
                                     verbose_name='Tiempo de Vigencia')
    date_vigencia = models.DateField(default=None, blank=True, null=True, verbose_name='Vencimiento de Documento')
    date_alert_exp = models.DateField(default=None, blank=True, null=True, verbose_name='Fecha Notificacion Proximo a Vencer')
    comment_review = models.CharField(max_length=250, null=True, blank=True, verbose_name='Observaciones Revisión')
    comment_aprove = models.CharField(max_length=250, null=True, blank=True, verbose_name='Observaciones Aprobación')
    empresa = models.CharField(max_length=50, default='Laboratorios H&B SAS', verbose_name='Compañia', null=True, blank=True)
    history = HistoricalRecords(custom_model_name=lambda x:f'Audit_{x}')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.last_name = None
        self.first_name = None

    def __str__(self):
        return self.description

    def toJSON(self):
        item = model_to_dict(self)
        item['tipodoc'] = self.tipodoc.toJSON()
        item['proceso'] = self.proceso.toJSON()
        item['filepdf'] = self.get_pdf()
        item['fileword'] = self.get_status_display()
        item['anexo'] = self.get_status_anexo()
        item['anexo_dos'] = self.get_status_anexo_dos()
        item["date_vigencia"] = None if self.date_vigencia is None else self.date_vigencia.strftime('%Y-%m-%d')
        item['date_alert_exp'] = None if self.date_alert_exp is None else self.date_alert_exp.strftime('%Y-%m-%d')
        item['date_creation'] = self.date_creation.strftime('%Y-%m-%d')
        item['date_updated'] = self.date_updated.strftime('%Y-%m-%d')
        item["date_revised"] = None if self.date_revised is None else self.date_revised.strftime('%Y-%m-%d')
        item['date_approved'] = None if self.date_approved is None else self.date_approved.strftime('%Y-%m-%d')
        return item

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, *args, **kwargs):
        if self.date_approved is not None:
            diasv = self.time_vigencia
            intdiasv = int(diasv)
            self.date_vigencia = datetime.now() + timedelta(days=intdiasv)
        if self.date_vigencia is not None:
            self.date_alert_exp = self.date_vigencia - timedelta(days=30)
        user = get_current_user()
        if user is not None:
            if not self.pk:
                self.user_creation = user
            else:
                self.user_updated = user
        super(Document, self).save()
        self.description = self.description.capitalize()
        self.code = self.code.upper()
        return super(Document, self).save(*args, **kwargs)

    def get_pdf(self):
        if self.filepdf:
            return '{}{}'.format(MEDIA_URL, self.filepdf)
        return ''

    def get_status_display(self):
        if self.fileword:
            return '{}{}'.format(MEDIA_URL, self.fileword)
        return ''

    def get_status_anexo(self):
        if self.anexo:
            return '{}{}'.format(MEDIA_URL, self.anexo)
        return ''

    def get_status_anexo_dos(self):
        if self.anexo_dos:
            return '{}{}'.format(MEDIA_URL, self.anexo_dos)
        return ''

    class Meta:
        verbose_name = 'Documento'
        verbose_name_plural = 'Documentos'
        db_table = 'Documentos'
        ordering = ['-date_updated']


class Process(BaseModel):
    process = models.CharField(max_length=60, verbose_name='Descripción', unique=True)
    codeprocess = models.CharField(max_length=10, verbose_name='Código', unique=True)
    leader = models.ForeignKey(User, blank=True, verbose_name='Líder de Proceso', on_delete=models.CASCADE)
    history = HistoricalRecords(custom_model_name=lambda x:f'Audit_{x}')

    def __str__(self):
        return self.process

    def toJSON(self):
        item = model_to_dict(self)
        item['leader'] = self.leader.tojson()
        return item

    class Meta:
        db_table = 'Proceso'
        verbose_name = 'Proceso'
        verbose_name_plural = 'Procesos'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, *args, **kwargs):
        user = get_current_user()
        if user is not None:
            if not self.pk:
                self.user_creation = user
            else:
                self.user_updated = user
        self.process = self.process.capitalize()
        self.codeprocess = self.codeprocess.upper()
        return super(Process, self).save(*args, **kwargs)
