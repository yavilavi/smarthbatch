from django.forms import *
from core.erp.models import Type, Document, Process
from core.user.models import User


class TypeForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'
            form.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Type
        fields = 'typedoc', 'codetype', 'validity'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # validacion de error/formulario
    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['codetype']) >= 5:
    #         self.add_error('codetype', 'Máximo 5 caractéres')
    #     return cleaned


class DocsForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['creator'].queryset = User.objects.filter(
            is_active=True).filter(groups__name__in=['Gestor', 'Administrador'])
        self.fields['reviewer'].queryset = User.objects.filter(
            is_active=True).filter(groups__name__in=['Gestor', 'Administrador'])
        self.fields['owner'].queryset = User.objects.filter(
            is_active=True).filter(groups__name__in=['Gestor', 'Administrador'])
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'tipodoc', 'version', 'time_vigencia', 'proceso', 'creator', 'reviewer', 'owner', \
                 'fileword', 'anexo', 'anexo_dos', 'numberchange', 'status'
        widgets = {
            'fileword': FileInput(attrs={'class': 'form-control-file', 'required': True}),
            'anexo': FileInput(attrs={'class': 'form-control-file'}),
            'anexo_dos': FileInput(attrs={'class': 'form-control-file'}),
            'code': TextInput(attrs={'class': 'form-control', 'required': True}),
            'description': TextInput(attrs={'class': 'form-control', 'required': True}),
            'tipodoc': Select(attrs={'class': 'form-control', 'required': True}),
            'version': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'status': Select(attrs={'class': 'form-control', 'readonly': True}),
            'time_vigencia': Select(attrs={'class': 'form-control', 'required': True}),
            'proceso': Select(attrs={'class': 'form-control', 'required': True}),
            'creator': Select(attrs={'class': 'form-control', 'required': True}),
            'reviewer': Select(attrs={'class': 'form-control', 'required': True}),
            'owner': Select(attrs={'class': 'form-control', 'required': True}),
            'numberchange': NumberInput(attrs={'class': 'form-control', 'readonly': True}),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class DocseditForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['creator'].queryset = User.objects.filter(is_active=True)
        self.fields['reviewer'].queryset = User.objects.filter(
            is_active=True).filter(groups__name__in=['Gestor', 'Administrador'])
        self.fields['owner'].queryset = User.objects.filter(
            is_active=True).filter(groups__name__in=['Gestor', 'Administrador'])
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'tipodoc', 'version', 'status', 'proceso', 'creator', 'reviewer', 'owner', \
                 'fileword', 'filepdf', 'anexo', 'anexo_dos'
        widgets = {
            'filepdf': FileInput(attrs={'class': 'form-control-file'}),
            'fileword': FileInput(attrs={'class': 'form-control-file'}),
            'anexo': FileInput(attrs={'class': 'form-control-file'}),
            'anexo_dos': FileInput(attrs={'class': 'form-control-file'}),
            'code': TextInput(attrs={'class': 'form-control'}),
            'description': TextInput(attrs={'class': 'form-control'}),
            'tipodoc': Select(attrs={'class': 'form-control'}),
            'version': TextInput(attrs={'class': 'form-control'}),
            'status': Select(attrs={'class': 'form-control'}),
            'proceso': Select(attrs={'class': 'form-control'}),
            'creator': Select(attrs={'class': 'form-control'}),
            'reviewer': Select(attrs={'class': 'form-control'}),
            'owner': Select(attrs={'class': 'form-control'}),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class DocsRevForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['status'].label = 'Nuevo Estado'
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'status', 'review', 'date_revised', 'comment_review'
        widgets = {
            'code': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'description': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'status': Select(attrs={'class': 'form-control', 'readonly': True}),
            'review': NullBooleanSelect(attrs={'class': 'form-control', 'readonly': True}),
            'date_revised': DateInput(attrs={'class': 'form-control', 'readonly': True}),
            'comment_review': Textarea(attrs={'class': 'form-control', 'rows': 3, 'required': True}),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class DocsAprovForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['status'].label = 'Nuevo Estado'
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'approval', 'version', 'status', 'date_approved', 'comment_review', \
                 'comment_aprove'
        widgets = {
            'code': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'description': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'approval': NullBooleanSelect(attrs={'class': 'form-control', 'readonly': True}),
            'version': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'status': Select(attrs={'class': 'form-control', 'readonly': True}),
            'date_approved': DateInput(attrs={'class': 'form-control', 'readonly': True}),
            'comment_review': Textarea(attrs={'class': 'form-control', 'rows': 3, 'readonly': True}),
            'comment_aprove': Textarea(attrs={'class': 'form-control', 'rows': 3, 'required': True})
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class DocsDevForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['comment_review'].label = 'Razón de Devolución'
        self.fields['status'].label = 'Nuevo Estado'
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'status', 'review', 'approval', 'comment_review'
        widgets = {
              'code': TextInput(attrs={'class': 'form-control', 'readonly': True}),
              'description': TextInput(attrs={'class': 'form-control', 'readonly': True}),
              'review': NullBooleanSelect(attrs={'class': 'form-control', 'readonly': True}),
              'approval': NullBooleanSelect(attrs={'class': 'form-control', 'readonly': True}),
              'status': Select(attrs={'class': 'form-control', 'readonly': True}),
              'comment_review': Textarea(attrs={'class': 'form-control', 'rows': 4, 'required': True})
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class DocsAdjustForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'tipodoc', 'version', 'status', 'proceso', 'fileword', 'anexo', 'anexo_dos'
        widgets = {
            'fileword': FileInput(attrs={'class': 'form-control-file'}),
            'anexo': FileInput(attrs={'class': 'form-control-file'}),
            'anexo_dos': FileInput(attrs={'class': 'form-control-file'}),
            'code': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'description': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'tipodoc': Select(attrs={'class': 'form-control', 'readonly': True}),
            'version': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'status': Select(attrs={'class': 'form-control', 'readonly': True}),
            'proceso': Select(attrs={'class': 'form-control', 'readonly': True}),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    def clean(self):
        cleaned = super().clean()
        if len(cleaned['code']) >= 15:
            self.add_error('code', 'Máximo 15 caracteres')
        return cleaned


class ProcessForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['leader'].queryset = User.objects.filter(is_active=True).filter(
            groups__name__in=['Gestor', 'Administrador'])
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'
            form.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Process
        fields = 'process', 'codeprocess', 'leader'

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


class ChangeForms(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['version'].label = 'Nueva Versión'
        for form in self.visible_fields():
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Document
        fields = 'code', 'description', 'changes', 'justification', 'version', 'numberchange', 'fileword', 'anexo', \
                 'anexo_dos'
        widgets = {
            'changes': Textarea(attrs={'class': 'form-control', 'required': True}),
            'justification': Textarea(attrs={'class': 'form-control', 'required': True}),
            'fileword': FileInput(attrs={'class': 'form-control-file', 'required': True}),
            'anexo': FileInput(attrs={'class': 'form-control-file'}),
            'anexo_dos': FileInput(attrs={'class': 'form-control-file'}),
            'version': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'numberchange': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'code': TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'description': TextInput(attrs={'class': 'form-control', 'readonly': True}),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
