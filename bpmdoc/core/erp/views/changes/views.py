from datetime import datetime
from random import randint

from django.contrib import messages
from django.shortcuts import redirect, render

from core.erp.models import Document
from core.erp.forms import ChangeForms



def document_change(request, id_change):
    document = Document.objects.get(id=id_change)
    document.id = None
    document.filepdf = None
    document.anexo = None
    document.anexo_dos = None
    document.justification = None
    document.approval= False
    document.review = False
    document.comment_aprove = None
    document.comment_review = None
    document.date_revised = datetime.now()
    document.changes = None
    document.status = 'En Revisión'
    document.creator = request.user
    document.version += 1
    consecutivo_numberdoc = numberchangerandom(5)
    document.numberchange = consecutivo_numberdoc
    if request.method == 'GET':
        form = ChangeForms(instance=document)
    else:
        version = request.POST.get('version')
        document.version = version
        changes = request.POST.get('changes')
        document.changes = changes
        justification = request.POST.get('justification')
        document.justification = justification
        numberchange = request.POST.get('numberchange')
        document.numberchange = numberchange
        document.save()
        messages.success (request, f'Cambio del documento "{document.code} {document.description}" enviado a ruta de aprobación!')
        return redirect('erp:docs_gest_list')

    context = {
        'form': form,
        'count_aprobar': Document.objects.filter(status='En Aprobación').filter(owner=request.user).count(),
        'count_elaboracion': Document.objects.filter(status='En Elaboración').filter(creator=request.user).count(),
        'count_revisar': Document.objects.filter(status='En Revisión').filter(reviewer=request.user).count(),
        'count_vencido': Document.objects.filter(status='Vencido').filter(owner=request.user).count(),
        'count_por_vencer': Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=request.user).count(),
        'count_total': 'count_aprobar' + 'count_vencido' + 'count_elaboracion' + 'count_revisar' + 'count_por_vencer'
    }
    return render(request, 'changes/create.html', context)


def numberchangerandom(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    numbase = 10**n
    return numbase+randint(range_start, range_end)
