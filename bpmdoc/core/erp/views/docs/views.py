from datetime import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import JsonResponse

from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView, DetailView, TemplateView
from openpyxl import Workbook
from django.http.response import HttpResponse
from openpyxl.styles import Alignment, Border, Side, PatternFill, Font

from core.erp.forms import *
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.views.changes.views import numberchangerandom

# Listado de documentos para grupo Administrador
class DocListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Document
    template_name = 'docs/list.html'
    permission_required = 'erp.add_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.all().order_by('-date_approved', 'description'):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_queryset(self):
        return Document.objects.filter(date_vigencia__lte=datetime.now()).update(status='Vencido')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['tittle'] = 'Maestro de Documentos'
        context['create_url'] = reverse_lazy('erp:docs_create')
        context['list_url'] = reverse_lazy('erp:docs_list')
        context['entity'] = 'Maestro de Documentos'
        return context

# Listado de documentos para grupo Gestor
class DocGestListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Document
    template_name = 'docs/list_gestion.html'
    permission_required = 'erp.change_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.filter(Q(status='Vigente') | Q(status='Vencido')):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_queryset(self):
        return Document.objects.filter(date_vigencia__lte=datetime.now()).update(status='Vencido')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Documentos'
        context['entity'] = 'Listado de Documentos'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:docs_gest_list')
        return context

# Lista de documentos para grupo User
class ConsulDocListView(LoginRequiredMixin, ListView):
    model = Document
    template_name = 'docs/listconsulta.html'
    permission_required = 'erp.view_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.filter(Q(status='Vigente') | Q(status='Vencido')):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_queryset(self):
        return Document.objects.filter(date_vigencia__lte=datetime.now()).update(status='Vencido')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Documentos'
        context['entity'] = 'Consulta de Documentos'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:consultdocs_list')
        return context

# Creación de documentos
class DocCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Document
    form_class = DocsForms
    initial = {'numberchange': numberchangerandom(6)}
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_list')
    permission_required = 'erp.add_document'
    url_redirect = success_url

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    code_name = form.cleaned_data.get('code')
                    doc_name = form.cleaned_data.get('description')
                    messages.success(request, f'Documento "{code_name} {doc_name}" creado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha ingresado información en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Nuevo Documento'
        context['list_url'] = reverse_lazy('erp:docs_list')
        context['entity'] = 'Nuevo Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['action'] = "add"
        return context

# Edición de documentos desde listado maestro
class DocUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Document
    form_class = DocseditForms
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_list')
    permission_required = 'erp.change_document'
    url_redirect = success_url

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.method = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    code_name = form.cleaned_data.get('code')
                    doc_name = form.cleaned_data.get('description')
                    messages.success(request, f'Documento "{code_name} {doc_name}" editado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No se han editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Editar Documento'
        context['entity'] = 'Editar Documentos'
        context['action'] = 'edit'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:docs_list')
        return context

# Eliminación de documentos
class DocDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Document
    form_class = DocsForms
    template_name = 'docs/delete.html'
    success_url = reverse_lazy('erp:docs_list')
    permission_required = 'erp.delete_document'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Eliminar Documento'
        context['entity'] = 'Documentos'
        context['entity_delete'] = 'documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:docs_list')
        return context


class DocFormView(LoginRequiredMixin, FormView):
    form_class = DocsForms
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_list')

    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Form | Doc'
        context['list_url'] = reverse_lazy('erp:docs_list')
        context['entity'] = 'Documentos'
        context['action'] = 'add'
        return context

# Detalle de documentos
class DocsDetailView(LoginRequiredMixin, DetailView):
    model = Document
    template_name = 'docs/detail.html'

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(DocsDetailView, self).get_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Detalle Documento'
        context['entity'] = 'Detalle Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:docs_list')
        return context

# Listado de documentos por revisar por usuario asignado (reviewer)
class DocRevListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Document
    template_name = 'docs/list_review.html'
    permission_required = 'erp.change_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.filter(status='En Revisión').filter(reviewer=self.request.user):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Rev. Documentos'
        context['list_url'] = reverse_lazy('erp:docs_review_list')
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['entity'] = 'Documentos por Revisar'
        return context

# Listado de documentos por aprobar por usuario asignado (owner)
class DocAproListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Document
    template_name = 'docs/list_review.html'
    permission_required = 'erp.change_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.filter(status='En Aprobación').filter(owner=self.request.user):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Aprobar Documentos'
        context['list_url'] = reverse_lazy('erp:docs_review_list')
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['entity'] = 'Documentos por Aprobar'
        return context

# Listado de documentos devueltos (En elaboracion) para ajustar de acuerdo a revisión en ruta de aprobación
class DocCreatedListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Document
    template_name = 'docs/list_created.html'
    permission_required = 'erp.change_document'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Document.objects.filter(status='En Elaboración').filter(creator=self.request.user):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Ajustar Documento'
        context['list_url'] = reverse_lazy('erp:docs_created_list')
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['entity'] = 'Documentos en Elaboración'
        return context

# Formulario para envío de documento para ruta de aprobación por parte del creador (creator) del documento
class DocAdjustUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Document
    form_class = DocsAdjustForms
    template_name = 'docs/create.html'
    initial = {'status': 'En Revisión'}
    success_url = reverse_lazy('erp:docs_created_list')
    permission_required = 'erp.change_document'
    url_redirect = success_url

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.method = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    code_name = form.cleaned_data.get('code')
                    doc_name = form.cleaned_data.get('description')
                    messages.success(request, f'Documento "{code_name} {doc_name}" enviado a Revisión!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Documento en Elaboración'
        context['list_url'] = reverse_lazy('erp:docs_created_list')
        context['entity'] = 'Documento en Elaboración'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context

# Formulario de Revisión/Aprobación del documento en ruta de aprobación por Revisor (reviewer)
class DocRevUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Document
    form_class = DocsRevForms
    initial = {'review': True, 'status': 'En Aprobación', 'date_revised': datetime.now()}
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_review_list')
    permission_required = 'erp.change_document'
    url_redirect = success_url

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.method = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    code_name = form.cleaned_data.get('code')
                    doc_name = form.cleaned_data.get('description')
                    messages.success(request, f'Documento "{code_name} {doc_name}" enviado a Aprobación!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Aprobación de Documento'
        context['list_url'] = reverse_lazy('erp:docs_review_list')
        context['entity'] = 'Aprobación de Documento (Revisión)'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context

# Formulario de Revisión/Aprobación del documento en ruta de aprobación por Aprobador (owner)
class DocAproUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Document
    form_class = DocsAprovForms
    initial = {'approval': True, 'status': 'Aprobado', 'date_approved': datetime.now()}
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_aprove_list')
    permission_required = 'erp.change_document'
    url_redirect = success_url

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.method = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    code_name = form.cleaned_data.get('code')
                    doc_name = form.cleaned_data.get('description')
                    messages.success(request, f'Documento "{code_name} {doc_name}" Aprobado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Aprob. Documento'
        context['list_url'] = reverse_lazy('erp:docs_aprove_list')
        context['entity'] = 'Aprobación de Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context

# Formulario de devolucion para ajustes del documento en ruta de aprobación por revisor o aprobador
class DocDevUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Document
    form_class = DocsDevForms
    initial = {'approval': False, 'status': 'En Elaboración', 'review': False}
    template_name = 'docs/create.html'
    success_url = reverse_lazy('erp:docs_review_list')
    permission_required = 'erp.change_document'
    url_redirect = success_url

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.method = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
                code_name = form.cleaned_data.get('code')
                doc_name = form.cleaned_data.get('description')
                messages.success(request, f'Documento "{code_name} {doc_name}" '
                                          f'devuelto a Elaboración, para ajustes según observaciones!')
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Dev. Documento'
        context['list_url'] = reverse_lazy('erp:docs_review_list')
        context['entity'] = 'Devolver Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
            context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'edit'
        return context

# Descarga de list en Excel
class ReportDocsExcel(TemplateView):
    def get(self, request, *args, **kwargs):
        documentos = Document.objects.filter(status='Vigente')
        wb = Workbook()
        ws = wb.active
        # Celda Titulo
        ws['B1'] = 'LISTADO MAESTRO DE DOCUMENTOS'
        ws['B1'].alignment = Alignment(horizontal='center', vertical='center')
        ws['B1'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['B1'].fill = PatternFill(start_color='D6DBDF', end_color='D6DBDF', fill_type='solid')
        ws['B1'].font = Font(name='Calibri', size='12', bold=True)
        ws.merge_cells('B1:I1')

        # Celdas cabecera de la tabla/lista
        ws['B3'] = 'CODIGO'
        ws['B3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['B3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['B3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['B3'].font = Font(name='Calibri', size='11', bold=True)
        ws['C3'] = 'DESCRIPCIÓN'
        ws['C3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['C3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['C3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['C3'].font = Font(name='Calibri', size='11', bold=True)
        ws['D3'] = 'TIPO'
        ws['D3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['D3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['D3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['D3'].font = Font(name='Calibri', size='11', bold=True)
        ws['E3'] = 'VERSION'
        ws['E3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['E3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['E3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['E3'].font = Font(name='Calibri', size='11', bold=True)
        ws['F3'] = 'PROCESO'
        ws['F3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['F3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['F3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['F3'].font = Font(name='Calibri', size='11', bold=True)
        ws['G3'] = 'LIDER'
        ws['G3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['G3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['G3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['G3'].font = Font(name='Calibri', size='11', bold=True)
        ws['H3'] = 'ESTADO'
        ws['H3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['H3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['H3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['H3'].font = Font(name='Calibri', size='11', bold=True)
        ws['I3'] = 'FECHA DE APROBADO'
        ws['I3'].alignment = Alignment(horizontal='center', vertical='center')
        ws['I3'].border = Border(left=Side(border_style='thin', color='566573'),
                                 right=Side(border_style='thin', color='566573'),
                                 top=Side(border_style='thin', color='566573'),
                                 bottom=Side(border_style='thin', color='566573')
                                 )
        ws['I3'].fill = PatternFill(start_color='EBEDEF', end_color='EBEDEF', fill_type='solid')
        ws['I3'].font = Font(name='Calibri', size='11', bold=True)

        cont = 4

        ws.row_dimensions[1].height = 25
        # Ancho de las celdas
        ws.column_dimensions['B'].width = 15
        ws.column_dimensions['C'].width = 35
        ws.column_dimensions['D'].width = 20
        ws.column_dimensions['E'].width = 10
        ws.column_dimensions['F'].width = 25
        ws.column_dimensions['G'].width = 25
        ws.column_dimensions['H'].width = 20
        ws.column_dimensions['I'].width = 20
        # Datos de la lista/tabla
        for documento in documentos:
            ws.cell(row=cont, column=2).value = documento.code
            ws.cell(row=cont, column=2).alignment = Alignment(horizontal='center', vertical='center')
            ws.cell(row=cont, column=2).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=2).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=3).value = documento.description
            ws.cell(row=cont, column=3).alignment = Alignment(horizontal='left', vertical='center', wrap_text=True)
            ws.cell(row=cont, column=3).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=3).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=4).value = documento.tipodoc.typedoc
            ws.cell(row=cont, column=4).alignment = Alignment(horizontal='center', vertical='center')
            ws.cell(row=cont, column=4).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=4).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=5).value = documento.version
            ws.cell(row=cont, column=5).alignment = Alignment(horizontal='center', vertical='center')
            ws.cell(row=cont, column=5).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=5).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=6).value = documento.proceso.process
            ws.cell(row=cont, column=6).alignment = Alignment(horizontal='left', vertical='center', wrap_text=True)
            ws.cell(row=cont, column=6).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=6).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=7).value = documento.owner.cargo
            ws.cell(row=cont, column=7).alignment = Alignment(horizontal='left', vertical='center', wrap_text=True)
            ws.cell(row=cont, column=7).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=7).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=8).value = documento.status
            ws.cell(row=cont, column=8).alignment = Alignment(horizontal='center', vertical='center')
            ws.cell(row=cont, column=8).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=8).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            ws.cell(row=cont, column=9).value = documento.date_approved
            ws.cell(row=cont, column=9).alignment = Alignment(horizontal='center', vertical='center')
            ws.cell(row=cont, column=9).font = Font(name='Calibri', size='11')
            ws.cell(row=cont, column=9).border = Border(
                left=Side(border_style='thin', color='566573'),
                right=Side(border_style='thin', color='566573'),
                top=Side(border_style='thin', color='566573'),
                bottom=Side(border_style='thin', color='566573')
            )

            cont += 1

        nombre_archivo = 'Listado_Maestro_Documentos.xlsx'
        response = HttpResponse(content_type='application/ms-excel')
        content = 'attachment; filename = {0}'.format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response
