from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import Type, Document
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from core.erp.forms import TypeForms


class TypedocListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Type
    template_name = 'typedoc/list.html'
    permission_required = 'erp.view_type'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Type.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Tipos de Documento'
        context['entity'] = 'Tipos de Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['create_url'] = reverse_lazy('erp:type_create')
        return context


class TypeCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Type
    form_class = TypeForms
    template_name = 'typedoc/create.html'
    success_url = reverse_lazy('erp:typedoc_list')
    permission_required = 'erp.add_type'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_type = form.cleaned_data.get('typedoc')
                    messages.success(request, f'Tipo de documento "{name_type}" creado satisfactoriamente!')
                else:
                    messages.error (request, form.errors)
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Crear Tipo de Documento'
        context['entity'] = 'Tipos de Documento'
        context['action'] = 'add'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy ('erp:typedoc_list')
        return context


class TypeUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Type
    form_class = TypeForms
    template_name = 'typedoc/create.html'
    success_url = reverse_lazy('erp:typedoc_list')
    permission_required = 'erp.change_type'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_type = form.cleaned_data.get('typedoc')
                    messages.success(request, f'Tipo de documento "{name_type}" se ha actualizado satisfactoriamente!')
                else:
                    messages.error (request, form.errors)
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Editar Tipo de Documento'
        context['entity'] = 'Editar Tipo de Documento'
        context['action'] = 'edit'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy ('erp:typedoc_list')
        return context


class TypeDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Type
    form_class = TypeForms
    template_name = 'typedoc/delete.html'
    success_url = reverse_lazy('erp:typedoc_list')
    permission_required = 'erp.delete_type'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Eliminar Tipo de Documento'
        context['entity'] = 'Tipos de Documento'
        context['entity_delete'] = 'tipo de Documento'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy ('erp:typedoc_list')
        return context


class TypeFormView(LoginRequiredMixin, FormView):
    form_class = TypeForms
    template_name = 'typedoc/create.html'
    success_url = reverse_lazy('erp:typedoc_list')

    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Form | Tipo Doc'
        context['list_url'] = reverse_lazy('erp:typedoc_list')
        context['entity'] = 'Tipos de Documento'
        context['action'] = 'add'
        return context
