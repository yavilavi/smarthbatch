from datetime import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import Process, Document
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from core.erp.forms import ProcessForms


class ProcessListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Process
    template_name = 'process/list.html'
    permission_required = 'erp.view_process'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Process.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Procesos'
        context['entity'] = 'Procesos'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['create_url'] = reverse_lazy('erp:process_create')
        return context


class ProcessCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Process
    form_class = ProcessForms
    template_name = 'process/create.html'
    success_url = reverse_lazy('erp:process_list')
    permission_required = 'erp.add_process'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_process = form.cleaned_data.get('process')
                    messages.success(request, f'El proceso "{name_process}" se ha creado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha ingresado datos en los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Crear Proceso'
        context['list_url'] = reverse_lazy('erp:process_list')
        context['entity'] = 'Crear Proceso'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['action'] = 'add'
        return context


class ProcessUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Process
    form_class = ProcessForms
    template_name = 'process/create.html'
    success_url = reverse_lazy('erp:process_list')
    permission_required = 'erp.change_process'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                if form.is_valid():
                    data = form.save()
                    name_process = form.cleaned_data.get('process')
                    messages.success(request, f'Proceso "{name_process}" actualizado satisfactoriamente!')
                else:
                    messages.error(request, form.errors)
            else:
                data['error'] = 'No ha editado los campos'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Editar Proceso'
        context['entity'] = 'Editar Proceso'
        context['action'] = 'edit'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:process_list')
        return context


class ProcessDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Process
    form_class = ProcessForms
    template_name = 'process/delete.html'
    success_url = reverse_lazy('erp:process_list')
    permission_required = 'erp.delete_process'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Eliminar Proceso'
        context['entity'] = 'Eliminar Proceso'
        context['entity_delete'] = 'proceso'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        context['list_url'] = reverse_lazy('erp:process_list')
        return context


class ProcessFormView(LoginRequiredMixin, FormView):
    form_class = ProcessForms
    template_name = 'process/create.html'
    success_url = reverse_lazy('erp:process_list')

    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.is_valid())
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tittle'] = 'Form | Proceso'
        context['list_url'] = reverse_lazy('erp:process_list')
        context['entity'] = 'Proceso'
        context['action'] = 'add'
        return context
