from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from core.erp.models import Document


class InicioView(LoginRequiredMixin, TemplateView):
    template_name = 'inicio.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['panel'] = 'panel del administrador'
        context['tittle'] = 'Inicio'
        context['entity'] = 'Inicio'
        context['count_vencido'] = Document.objects.filter(status='Vencido').filter(
            owner=self.request.user).count()
        context['count_aprobar'] = Document.objects.filter(status='En Aprobación').filter(
            owner=self.request.user).count()
        context['count_elaboracion'] = Document.objects.filter(status='En Elaboración').filter(
            creator=self.request.user).count()
        context['count_revisar'] = Document.objects.filter(status='En Revisión').filter(
            reviewer=self.request.user).count()
        context['count_por_vencer'] = Document.objects.filter(status='Vigente').filter(
            date_alert_exp__lte=datetime.now()).filter(owner=self.request.user).count()
        context['count_total'] = context['count_aprobar'] + context['count_vencido'] + context['count_elaboracion'] + \
                                 context['count_revisar'] + context['count_por_vencer']
        return context
