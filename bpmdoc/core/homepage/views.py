from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'

class SinpermisosView(TemplateView):
    template_name = 'sinpermisos.html'